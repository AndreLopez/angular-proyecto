import { Component, OnInit } from '@angular/core';
import { ArtistsService } from '../services/artists.service';
import { SongsService } from '../services/songs.service'; 
import { AlbumsService } from '../services/albums.service'
import { NgForm } from '@angular/forms';
import { Artists } from '../models/artists';
import { Songs } from '../models/songs'; 
import { Albums } from '../models/albums'; 

declare var M: any;

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
  providers: [AlbumsService]
})
export class AlbumsComponent implements OnInit {

  constructor( private albumsService: AlbumsService) { }

  ngOnInit() {
    this.getAlbums(); 
   
  }

  getAlbums() {
    this.albumsService.getAlbums()
    .subscribe( res => {
      this.albumsService.albums = res as Albums[];
     
    }); 
  }

  addAlbums(form: NgForm) {
    if(form.value._id) {
      this.albumsService.putAlbums(form.value)
      .subscribe( res => {
        this.resetForm(form); 
        M.toast({html: 'Album Actualizado'}); 
        this.getAlbums(); 
    }); 
    } else {
      this.albumsService.postAlbums(form.value)
      .subscribe( res => {
        this.resetForm(form); 
        M.toast({html: 'Album Guardado'}); 
        this.getAlbums(); 
      }); 
     }
    }

    

    resetForm(form?: NgForm) {
      if (form){
        form.reset();
        this.albumsService.selectedAlbums = new Albums();
      }
  }

}
