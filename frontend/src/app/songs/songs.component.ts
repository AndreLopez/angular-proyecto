import { Component, OnInit } from '@angular/core';
import { SongsService } from '../services/songs.service';
import { NgForm } from '@angular/forms';
import { Songs } from '../models/songs';

declare var M: any;

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongsComponent implements OnInit {

  constructor(private songsService: SongsService) { }

  ngOnInit() {
    this.getSongs();
  }

  addSongs(form: NgForm) {
    if(form.value._id) {
      this.songsService.putSongs(form.value)
      .subscribe( res => {
        this.resetForm(form); 
        M.toast({html: 'Cancion Actualizada'}); 
        this.getSongs(); 
    }); 
    } else {
      this.songsService.postSongs(form.value)
      .subscribe( res => {
        this.resetForm(form); 
        M.toast({html: 'Cancion Guardada'}); 
        this.getSongs(); 
      }); 
     }
    }
    
    getSongs() {
      this.songsService.getSongs()
      .subscribe( res => {
        this.songsService.songs = res as Songs[];
       
      }); 
    }
   
    editSongs(songs: Songs) {
      this.songsService.selectedSongs = songs;
       
    }
  
    deleteSongs(_id: string) {
      if(confirm('¿Desea eliminar esta cancion?')) {
       this.songsService.deleteSongs(_id)
       .subscribe( res => {
         this.getSongs(); 
         }); 
       }
     }
  
  
    resetForm(form?: NgForm) {
        if (form){
          form.reset();
          this.songsService.selectedSongs = new Songs();
        }
    }
  
  }
  
