import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from '@angular/router';
import { ArtistsComponent } from 'src/app/artists/artists.component'; 
import { SongsComponent } from 'src/app/songs/songs.component';
import { AlbumsComponent} from 'src/app/albums/albums.component';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch:'full'},
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
 
  { path: 'songs', component: SongsComponent}, 
  { path: 'artist', component: ArtistsComponent }, 
  { path: 'album', component: AlbumsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class  AuthRoutingModule { }
