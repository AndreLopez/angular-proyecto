import { Component, OnInit } from '@angular/core';
import { ArtistsService } from '../services/artists.service';
import { NgForm } from '@angular/forms';
import { Artists } from '../models/artists';

declare var M: any;

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.css'],
  providers: [ ArtistsService]
})
export class ArtistsComponent implements OnInit {

  constructor(private artistsService: ArtistsService) { }

  ngOnInit() {
    this.getArtists();
  }

  addArtists(form: NgForm) {
  if(form.value._id) {
    this.artistsService.putArtists(form.value)
    .subscribe( res => {
      this.resetForm(form); 
      M.toast({html: 'Artista Actualizado'}); 
      this.getArtists(); 
  }); 
  } else {
    this.artistsService.postArtists(form.value)
    .subscribe( res => {
      this.resetForm(form); 
      M.toast({html: 'Artista Guardado'}); 
      this.getArtists(); 
    }); 
   }
  }
  
  getArtists() {
    this.artistsService.getArtists()
    .subscribe( res => {
      this.artistsService.artists = res as Artists[];
     
    }); 
  }
 
  editArtists(artists: Artists) {
    this.artistsService.selectedArtists = artists;
     
  }

  deleteArtists(_id: string) {
   if(confirm('¿Desea eliminar este artista?')) {
    this.artistsService.deleteArtists(_id)
    .subscribe( res => {
      this.getArtists(); 
      }); 
    }
  }


  resetForm(form?: NgForm) {
      if (form){
        form.reset();
        this.artistsService.selectedArtists = new Artists();
      }
  }

}
