import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Artists } from '../models/artists';

@Injectable({
  providedIn: 'root'
})
export class ArtistsService {

  selectedArtists: Artists;
  artists: Artists[]; 
  readonly URL_API = 'http://localost:3000/artist';

  constructor(private http: HttpClient) {
    
    this.selectedArtists = new Artists(); 
   
  }

  getArtists() {
    return this.http.get('http://localhost:3000/artist');
  }

  postArtists(Artists: Artists) {
    return this.http.post('http://localhost:3000/artist/create',  Artists);
  }

  putArtists(artists: Artists) {
     return this.http.put('http://localhost:3000/artist' + `/${artists._id}`, artists);
  }

  deleteArtists(_id: String) {
    return this.http.delete('http://localhost:3000/artist' + `/${_id}`); 
  }


}
