import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Albums } from '../models/albums';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  selectedAlbums: Albums;
  albums: Albums[]; 
  readonly URL_API = 'http://localost:3000/album';

  constructor(private http: HttpClient) {

    this.selectedAlbums = new Albums(); 
     

   }

   getAlbums() {
    return this.http.get('http://localhost:3000/album');
  }

  postAlbums(Albums: Albums) {
    return this.http.post('http://localhost:3000/album/create',  Albums);
  }

  putAlbums(albums: Albums) {
     return this.http.put('http://localhost:3000/album' + `/${albums._id}`, albums);
  }

  deleteAlbums(_id: String) {
    return this.http.delete('http://localhost:3000/album' + `/${_id}`); 
  }

}
