import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Songs } from '../models/songs'; 

@Injectable({
  providedIn: 'root'
})
export class SongsService {

  selectedSongs: Songs; 
  songs: Songs[]; 


  constructor(private http: HttpClient) {

    this.selectedSongs = new Songs(); 

   }

   getSongs() {
     return this.http.get('http://localhost:3000/songs'); 
   } 

   postSongs(Songs: Songs) {
    return this.http.post('http://localhost:3000/songs/create', Songs); 
   }

   putSongs(songs: Songs) {
    return this.http.put('http://localhost:3000/songs' + `/${songs._id}`, songs);
   }

   deleteSongs(_id: String) {
    return this.http.delete('http://localhost:3000/songs' + `/${_id}`); 
  }

}
