const express = require('express'); 
const router = express.Router(); 
const Songs = require('../song/song.controller'); 

router.get('/songs', Songs.getSong); 
router.get('/songs/:id', Songs.getSongs); 
router.post('/songs/create', Songs.createSongs); 
router.put('/songs/:id', Songs.editSongs);
router.delete('/songs/:id', Songs.deleteSongs); 

module.exports = router; 