const express = require('express'); 
const router = express.Router(); 
const Album = require('../album/album.controller');

router.get('/album', Album.getAlbums); 
router.post('/album/create', Album.createAlbum ); 
router.get('/album/:id', Album.getAlbum); 
router.put('/album/:id', Album.editAlbum); 
router.delete('/album/:id', Album.deleteAlbum); 

module.exports = router; 