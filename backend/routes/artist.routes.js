const express = require('express'); 
const router = express.Router(); 
const Artists = require('../artist/artist.controller');

router.get('/artist', Artists.getArtists);
router.post('/artist/create', Artists.createArtist); 
router.get('/artist/:id', Artists.getArtist); 
router.put('/artist/:id', Artists.editArtist);
router.delete('/artist/:id', Artists.deleteArtist); 


module.exports = router; 