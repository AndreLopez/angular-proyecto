const Artist = require('../models/artist.model'); 

exports.createArtist = async (req, res) => {

    const artist = new Artist({
      name: req.body.name
    }); 
     artist.save(); 
     //response
     res.json('Artista Guardado'); 


}

exports.getArtists = async (req, res) => { 
  const artist = await Artist.find();
  //response
  res.send(artist); 
}

exports.getArtist = async (req, res ) => {
   
  const artist = await Artist.findById(req.params.id); 
  res.send(artist); 
}

exports.editArtist = async (req, res) => {
    const {id} = req.params;
    const artist = {
      name: req.body.name
    };
     await  Artist.findByIdAndUpdate(id, {$set: artist }, {new: true}); 
      res.json('Artista actualizado'); 
};

exports.deleteArtist = async (req, res) => {
  await Artist.findByIdAndDelete(req.params.id); 
  res.json('Artista Eliminado')
}
