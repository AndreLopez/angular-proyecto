const Album = require('../models/album.model'); 


exports.getAlbums = async (req, res) => { 
  const albums = await Album.find();
  //response
  res.json(albums); 
}


exports.createAlbum = (req, res) => {
    const album = new Album(req.body); 
    album.save(); 
    //response
    res.json('Album Guardado'); 
}

exports.getAlbum = async (req, res) => {
  const album = await Album.findById(req.params.id); 
  res.json(album); 
};



exports.editAlbum = async (req, res) => {
    const {id} = req.params;
    const album = {
      name: req.body.name,
      artist: req.body.artist,
      songs: req.body.songs
    };
     await  Album.findByIdAndUpdate(id, {$set: album }, {new: true}); 
      res.json('Album actualizado'); 
};

exports.deleteAlbum = async (req,res) => {
  await Album.findByIdAndDelete(req.params.id); 
  res.json('Album Eliminado'); 
}