const Song = require('../models/song.model'); 


exports.getSong = async (req, res) => { 
    const artist = await Song.find();
    //response
    res.json(artist); 
}


exports.createSongs = (req, res) => {
    const songs = new Song({
        name: req.body.name
    });
    songs.save(); 
    //response
    res.json('Cancion Guardada'); 
}

exports.getSongs = async (req, res) => {
   
    const songs = await Song.findById(req.params.id); 
    res.json(songs); 
}

exports.editSongs = async (req, res) => {
    const {id} = req.params;
    const songs = {
      name: req.body.name
    };
     await  Song.findByIdAndUpdate(id, {$set: songs }, {new: true}); 
      res.json('Cancion actualizada'); 
};

exports.deleteSongs = async (req, res) => {
    await Song.findByIdAndDelete(req.params.id); 
    res.json('Cancion Eliminada'); 
}