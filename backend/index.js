'use strict' 
const cors = require('cors'); 
const express = require('express');
const path = require('path'); 
const session = require('express-session'); 
const methodOverride = require('method-override'); 


//Inicializacion 
const app = express(); 
const { monngose } = require('./config/database'); 
const router = express.Router(); 


require('./models/artist.model');
require('./models/song.model');

const bodyParser = require('body-parser'); 
const bodyParserJSON = bodyParser.json(); 
const bodyParserURLEnconded = bodyParser.urlencoded( { extended: true }); 

app.use(bodyParserJSON); 
app.use(bodyParserURLEnconded); 

app.use(cors({origin: 'http://localhost:4200'})); 
app.use(express.json());




//Settings 
app.set('port', process.env.PORT || 3000); 

//Middlewares
app.use(methodOverride('_method')); 

// Routes 
app.use(require('./routes/auth.routes')); 
app.use(require('./routes/album.routes')); 
app.use(require('./routes/song.routes')); 
app.use(require('./routes/artist.routes'));


// Server is listening
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});