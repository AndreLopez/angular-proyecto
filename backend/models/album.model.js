const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 
const Artist = mongoose.model('Artist'); 
const Songs = mongoose.model('Songs')

require('./artist.model'); 
require('./song.model'); 

const albumsSchema = new Schema ({
    name: {
        type: String, 
        required: true, 
    }, 

    year: {
        type: Number
    }, 

    artist: {
        type: Schema.ObjectId, ref: 'Artist' 
    }, 

    songs: {
        type: Schema.ObjectId, ref: 'Songs'
    }

}); 

module.exports = mongoose.model('Albums', albumsSchema); 